﻿using System;

namespace Booking.Core.Domain
{
    public abstract class BookingBase
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Date { get; set; }
    }
}
