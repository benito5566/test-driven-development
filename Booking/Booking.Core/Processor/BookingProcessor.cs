﻿using Booking.Core.Domain;
using System;
using System.Linq;

namespace Booking.Core.Processor
{
    public class BookingProcessor
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IRoomRepository _roomRepository;

        public BookingProcessor(IBookingRepository bookingRepository, IRoomRepository roomRepository)
        {
            _bookingRepository = bookingRepository;
            _roomRepository = roomRepository;
        }

        public BookingResult BookRoom(BookingRequest request)
        {
            _ = request ?? throw new ArgumentNullException(nameof(request));

            var availableRooms = _roomRepository.GetAvailableRooms(request.Date);

            if (availableRooms.FirstOrDefault() is Room)
            {
                _bookingRepository.Add(Create<Domain.Booking>(request));
            }

            return Create<BookingResult>(request);
        }

        private static T Create<T>(BookingBase request) where T : BookingBase, new()
        {
            return new T
            {
                Name = request.Name,
                LastName = request.LastName,
                Email = request.Email,
                Date = request.Date
            };
        }
    }
}