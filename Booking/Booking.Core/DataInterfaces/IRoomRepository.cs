﻿using System;
using System.Collections.Generic;

namespace Booking.Core.Processor
{
    public interface IRoomRepository
    {
        IEnumerable<Room> GetAvailableRooms(DateTime targetDate);
    }
}