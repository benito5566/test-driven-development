﻿namespace Booking.Core.Processor
{
    public interface IBookingRepository
    {
        void Add(Domain.Booking booking);
    }
}