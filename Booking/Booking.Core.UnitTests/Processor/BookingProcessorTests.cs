﻿using Booking.Core.Domain;
using NUnit.Framework;
using System;
using FakeItEasy;
using System.Collections.Generic;

namespace Booking.Core.Processor
{
    [TestFixture]
    public class BookingProcessorTests
    {
        private BookingProcessor _sut;

        private IBookingRepository _bookingRepository;

        private IRoomRepository _roomRepository;

        [SetUp]
        public void SetUp()
        {
            _bookingRepository = A.Fake<IBookingRepository>();
            _roomRepository = A.Fake<IRoomRepository>();
            _sut = new BookingProcessor(_bookingRepository, _roomRepository);
        }

        [Test]
        public void BookRoom_WhenRequestHasValue_ShouldReturnRequestValues()
        {
            var request = new BookingRequest
            {
                Name = "name",
                LastName = "Last Name",
                Email = "name@somewhere.com",
                Date = new DateTime(2020, 08, 11)
            };

            var result = _sut.BookRoom(request);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(result.Name, request.Name);
                Assert.AreEqual(result.LastName, request.LastName);
                Assert.AreEqual(result.Email, request.Email);
                Assert.AreEqual(result.Date, request.Date);
            });
        }

        [Test]
        public void BookRoom_WhenRequestIsNull_ShouldThrowNullArgumentException()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => _sut.BookRoom(null));

            StringAssert.AreEqualIgnoringCase("request", exception.ParamName);
        }

        [Test]
        public void BookRoom_WhenRequestIsValidAndARoomIsAvailable_ShouldSaveBooking()
        {
            var request = new BookingRequest
            {
                Name = "name",
                LastName = "Last name",
                Email = "name@something.com",
                Date = new DateTime(2020, 08, 14)
            };

            A.CallTo(() => _roomRepository.GetAvailableRooms(A<DateTime>._))
                .Returns(new List<Room> { new Room() });
            
            _sut.BookRoom(request);

            A.CallTo(() => _bookingRepository.Add(A<Domain.Booking>
                .That
                .Matches(booking => 
                   booking.Name == request.Name
                && booking.LastName == request.LastName
                && booking.Email == request.Email
                && booking.Date == request.Date)))
                .MustHaveHappened();
        }
 
        [Test]
        public void BookRoom_WhenNotAvailableRoom_ShouldReturn()
        {
            // Make sure a room is not available
            var request = new BookingRequest
            {
                Name = "name",
                LastName = "Last name",
                Email = "name@something.com",
                Date = new DateTime(2020, 08, 14)
            };

            A.CallTo(() => _roomRepository.GetAvailableRooms(A<DateTime>._)).Returns(new List<Room>());

            _sut.BookRoom(request);

            A.CallTo(() => _bookingRepository.Add(A<Domain.Booking>._)).MustNotHaveHappened();
        }
    }
}
